# dexdash

## Description
DexDash, a Dexcom CGM readout script to display values on Waveshare 7.5 eInk display. Includes a large value display and historical graph script.

## Installation
**First step**: Prepare a working Raspberry Pi. Development and testing is done on a Raspberry Pi Zero W Rev 1.1 (wih GPIO pins) running Raspbian 10 (Buster). Set up the wireless configuration. Connect the Waveshare Pi Hat to the GPIO pins.

**Second step**: Install supporting Python 3 libraries:
```
apt-get install python3-rpi.gpio python-imaging python-smbus python-dev
pip3 install pydexcom
```

**Third step**:Install the WaveShare e-Paper Python libraries:
```
git clone https://github.com/waveshare/e-Paper
cd e-Paper/RaspberryPi_JetsonNano/python
python setup.py install
```
OR
```
pip install waveshare-epaper
```

**Fourth step**: Try the example scripts for your display. The development, scaling and testing is done on the 7.5" Red e-Paper version so dimensions also limited to those specifications. Download the example script from [https://github.com/waveshare/e-Paper/blob/master/RaspberryPi_JetsonNano/python/examples/epd_7in5_V2_test.py] or go into the installation folder under the ```examples``` folder and run:

```
python3 epd_7in5_V2_test.py
```

**Fifth step**: clone the dexdash repository:
```
git clone https://gitlab.com/hendrikvb/dexdash
cd dexdash
```

**Fifth step**: Grab the font used from the Google TTF repository (download link at top right corner) [https://fonts.google.com/specimen/Comfortaa] and copy ```Comfortaa-Bold.ttf``` in the cloned git repository.

**Sixth step**: Configure and customize the script to your configuration. In ```dexdash.py```, change the CSV path and font path to the working folder (if you choose to move the 2 python scripts somewhere else, change it to that). The fontpath is the full path where the Comfortaa-Bold.ttf file will be located. Dexdash will generate CSV data. The CSVPath is the full path where this fille will be created (appropriate file permissions are needed). Also change the DexCom API settings:
```
dexcom = Dexcom("username", "password","ous=True") # add ous=True if outside of US
```
See instructions on [https://github.com/gagebenne/pydexcom] on getting these credentials.

In ```dexgraph.py```, update the same CSVPath and FontPath values (full path!).

## Screenshots
![Dexdash eInk in use](dexdash-frame.jpeg "Dexdash eInk in use")
![Dexdash Graph](dexdash-graph.jpg "Dexdash Graphing")

## Scheduling
dex-graph.py uses data stored by dexdash.py. Ideally, schedule the execution with a cronjob to run every 3 minutes (adjust where needed):
```
0,6,12,18,24,30,36,42,48,54 * * * * python3 /home/user/dexdash.py
3,9,15,21,27,33,39,45,51,57 * * * * python3 /home/user/dexgraph.py
```

## Authors and acknowledgment
This code uses [https://github.com/gagebenne/pydexcom] and [https://github.com/waveshare/e-Paper]. The code is based on the epd_7in5_V2_test.py example [https://github.com/waveshare/e-Paper/blob/master/RaspberryPi_JetsonNano/python/examples/epd_7in5_V2_test.py]. The example setup runs on a Raspberry Pi Zero W and the Waveshare 7.5 e-Paper display with Pi Hat [https://www.waveshare.com/product/7.5inch-e-paper-hat-b.htm]. Inspiration also taken from Simon Somlais work with the same hardware [https://simonsomlai.com/en/e-paper-quote-display-raspberry-pi]. The frame used is an Ikea Knoppang 13x18cm frame [https://www.ikea.com/gb/en/p/knoppaeng-frame-black-10387124/], adjusted to fit the circuits on the back, currently stuck on with masking tape.

Some code is based on earlier work by me, as part of the dexpi project. [https://gitlab.com/hendrikvb/dexpi]

## Contact
The best way to reach out is to send a [Twitter DM @hendrikvb](https://twitter.com/hendrikvb).

## License
Use at will! Be kind! Just don't take any medical decisions based on this project! USE AT OWN RISK!

## Project status
Initial functional build. Needs work