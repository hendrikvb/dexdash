#!/usr/bin/python

# Version 0.2 of DexDash - dexgraph, a Dexcom CGM readout script to display values on Waveshare 7.5 eInk display
# https://gitlab.com/hendrikvb/dexdash - Hendrik Van Belleghem - 07 April 2023

# -*- coding:utf-8 -*-
import sys
import os
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
from waveshare_epd import epd7in5_V2
import time
from PIL import Image,ImageDraw,ImageFont
from pydexcom import Dexcom
import csv
try:
    from itertools import izip
except ImportError: 
  izip = zip

csvpath = "/home/user/bglvalues.csv"
fontpath = "/home/user/Comfortaa-Bold.ttf"

bgl = []
allbgl = []
times = []
alltimes = []

izip = zip

with open(csvpath, newline='') as csvfile:
  reader = csv.DictReader(csvfile, delimiter=";",fieldnames=["bgl","time"])
  for row in reader:
    allbgl.append(int(row["bgl"]))
    alltimes.append(row["time"])
csvfile.close()

startx = 105 
starty = -15
radius = 10
increments = 32
datarange = 20
scale = 0.9

bgl = allbgl[-datarange:]
lastbgl = allbgl[-1]
times = alltimes[-datarange:]

with open(csvpath, 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=';',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for i, bglx in enumerate(bgl):
      csvwriter.writerow([bgl[i],times[i]])
    csvfile.close

font15 = ImageFont.truetype(fontpath,15)
font75 = ImageFont.truetype(fontpath,75)

try:
    epd = epd7in5_V2.EPD()
    # 7.5 inch V2 display is 800 * 480
    epd.init()
    epd.Clear()

    Himage = Image.new('1', (epd.width, epd.height), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Himage)
    # Vertical axis
    draw.line((round(startx*scale),round((epd.height-starty)*scale), round(startx*scale), round((epd.height-starty-400)*scale)), fill="black", width=3) 
    # Horizontal axis
    draw.line((round(startx*scale),round((epd.height-starty)*scale), round((startx+(increments*datarange)*scale)), round((epd.height-starty)*scale)), fill="black", width=3)
    for i in range(8):
      draw.line((round((startx-5)*scale),round((epd.height-starty-((i+1)*50))*scale), round((startx+(increments*datarange))*scale), round((epd.height-starty-((i+1)*50))*scale)), fill="black", width=1)
      draw.text( ( round((startx-20)*scale), round((epd.height-starty-((i+1)*50))*scale)), str((i+1)*50), font = font15, fill = 0) 

    draw.text((550,80), str(lastbgl), font = font75, fill = 0)

    for i, bglx in enumerate(bgl):
      # X starts at 80, increments with 32
      x = startx+(i*increments)
      # Y is height - Blood Glucose value (0-400) + 50 ->   480 - 100 + 50 = 
      y = epd.height - bglx - starty 
      texty = y - 25
      draw.ellipse((round(x*scale),round(y*scale),round((x+radius)*scale),round((y+radius)*scale)), fill = 'black', outline ='black')
      draw.text( (round(x*scale), round((y+10)*scale)), str(bglx), font = font15, fill = 0)

    epd.display(epd.getbuffer(Himage))

except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd7in5_V2.epdconfig.module_exit()
    exit()
