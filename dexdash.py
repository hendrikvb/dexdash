#!/usr/bin/python

# Version 0.2 of DexDash, a Dexcom CGM readout script to display values on Waveshare 7.5 eInk display
# https://gitlab.com/hendrikvb/dexdash - Hendrik Van Belleghem - 07 April 2023

# -*- coding:utf-8 -*-
import sys
import os
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
from waveshare_epd import epd7in5_V2
import time
from PIL import Image,ImageDraw,ImageFont
import requests
import traceback
from pydexcom import Dexcom
import csv

banner = "DexDash Blood Glucose"
fontpath = "/home/user/Comfortaa-Bold.ttf"

def get_reading():
  dexcom = Dexcom("username", "password","ous=True") # add ous=True if outside of US
  trends = ["","Rising Quickly","Rising","Rising Slightly","Steady","Falling Slightly","Falling","Falling Quickly","Unable to determine trend","Trend Unavailable"]
  bg = dexcom.get_current_glucose_reading()
  bgl = 0
  bgltime = ""
  bgltrend = 0 
  try:
    bgl = bg.value
    bgltime = bg.time 
    bgltrend = bg.trend
  except TypeError:
    bgl = 0 
    bgltime = ""
    bgltrend = 8
    print("Unknown reading value!")
 
  with open(csvpath, 'a', newline='') as csvfile:
     csvwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
     csvwriter.writerow([str(bgl), str(bgltime)])
  csvfile.close()
  return str(bgl), str(bgltime), trends[bgltrend]

try:
    epd = epd7in5_V2.EPD()

    epd.init()
    epd.Clear()

    bgl, date, trend = get_reading()
    font325 = ImageFont.truetype(fontpath,325)
    font100 = ImageFont.truetype(fontpath,105)
    font70 = ImageFont.truetype(fontpath,70)
    font50 = ImageFont.truetype(fontpath,50)
    font35 = ImageFont.truetype(fontpath,35)

    # Drawing on the Horizontal image
    Himage = Image.new('1', (epd.width, epd.height), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Himage)
    # Banner
    draw.text((80,40),banner,font = font50, fill = 0) 
    # Top line
    draw.text((90, 90), bgl, font = font325, fill = 0)

    # Trend indication in lower right corner
    if trend == "Rising Slightly":
      draw.text((615,380), "\u2191", font = font70, fill = 0)
    if trend == "Rising": 
      draw.text((605,380), "\u2191\u2191", font = font70, fill = 0)
    if trend == "Rising Quickly": 
      draw.text((595,380), "\u2191\u2191\u2191", font = font70, fill = 0)

    if trend == "Falling Slightly":
      draw.text((615,380), "\u2193", font = font70, fill = 0)
    if trend == "Falling":
      draw.text((605,380), "\u2193\u2193", font = font70, fill = 0)
    if trend == "Falling Quickly":
      draw.text((595,380), "\u2193\u2193\u2193", font = font70, fill = 0)

    if trend == "Steady":
       draw.text((615,385), "\u2192", font = font70, fill = 0)
    if trend == "Trend Unavailable":
       draw.text((615,385), "?", font = font70, fill = 0)
    if trend == "Unable to determine trend":
       draw.text((615,385), "?", font = font70, fill = 0)

    # Bottom line
    draw.text((80, 410), "mg/dL", font = font35, fill = 0)
    draw.text((230,410), date, font = font35, fill = 0) 
    epd.display(epd.getbuffer(Himage))

except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd7in5_V2.epdconfig.module_exit()
    exit()
